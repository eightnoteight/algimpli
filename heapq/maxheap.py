#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from heapq import _siftdown_max
from heapq import _siftup_max
from heapq import _heapify_max
from heapq import _heappushpop_max
heapify = _heapify_max
heappushpop = _heappushpop_max

def heappop(heap):
    lastelt = heap.pop()
    if heap:
        returnitem = heap[0]
        heap[0] = lastelt
        _siftup_max(heap, 0)
    else:
        returnitem = lastelt
    return returnitem

def heappush(heap, item):
    heap.append(item)
    _siftdown_max(heap, 0, len(heap)-1)

if __name__ == '__main__':
    import random
    pool = []
    arr = [random.randrange(10**12) for _ in xrange(10**6)]
    for x in xrange(10**6):
        heappush(pool, arr[x])
    for x in xrange(10**6):
        heappop(pool)
