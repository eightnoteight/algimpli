#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,bad-builtin,missing-docstring

class unionfind(object):
    def __init__(self, n):
        self._number_of_nodes, self._count = n, n
        self._roots, self._weights = list(xrange(n)), [1]*n

    def union(self, a, b):
        self[a] = b

    def find(self, key):
        return self[key]

    def is_connected(self, a, b):
        return self[a, b]

    def __len__(self):
        return self._count

    def __setitem__(self, a, b):
        aroot, broot = self[a], self[b]
        if aroot == broot:
            return
        if self._weights[aroot] > self._weights[broot]:
            aroot, broot = broot, aroot
        self._roots[aroot] = broot
        self._weights[broot] += self._weights[aroot]
        self._weights[aroot] = 0
        self._count -= 1

    def __getitem__(self, key):
        if isinstance(key, tuple):
            tmp = self[key[0]]
            for x in xrange(1, len(key)):
                if self[key[x]] != tmp:
                    return False
            return True
        if isinstance(key, slice):
            return [self[x] for x in xrange(*key.indices(self._number_of_nodes))]
        roots = self._roots
        while roots[key] != key:
            roots[key] = roots[roots[key]]
            key = roots[key]
        return key


def main(dstrea):
    uf = unionfind(10**6)
    for _ in xrange(10**6):
        if next(dstrea):
            uf[next(dstrea)] = next(dstrea)
        else:
            _ = uf[next(dstrea)]


if __name__ == '__main__':
    from cProfile import run
    from random import randrange
    dstream = []
    for _ in xrange(10**6):
        dstream.append(randrange(2))
        if dstream[-1]:
            dstream.append(randrange(10**6))
            dstream.append(randrange(10**6))
        else:
            dstream.append(randrange(10**6))
    dstream = iter(dstream)
    run('main(dstream)')
