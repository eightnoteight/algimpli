#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from array import array

class quickfind(object):
    def __init__(self, n, dt='I'):
        self._length = n
        self._roots = array(dt, list(xrange(n)))
        self._count = n

    def union(self, a, b):
        roots = self._roots
        a, b = roots[a], roots[b]
        if a == b:
            return
        self._count -= 1
        for x in xrange(len(roots)):
            if roots[x] == b:
                roots[x] = a

    def find_root(self, a):
        return self._roots[a]

    def get_count(self):
        return self._count

    def is_connected(self, a, b):
        return self._roots[a] == self._roots[b]



