#!/usr/bin/env python
# -*- encoding: utf-8 -*-


def updateval(segt, arr, ind, val):
    """
    :returns: None
    """
    diff = val - arr[ind]
    arr[ind] = val

    def _updateval(segt, lo, hi, diff, ind):
        if ind < lo or ind >= hi:
            return
        segt[ind] += diff
        mid = lo + (hi - lo) // 2
        _updateval(segt, lo, mid, diff, 2*ind + 1)
        _updateval(segt, mid, hi, diff, 2*ind + 2)
    _updateval(segt, 0, len(arr), diff, 0)


def getsum(segt, arr, qbeg, qend):
    """
    :returns: sum over the given query range.
    """
    def _getsum(segt, beg, end, qbeg, qend, ind):
        if beg >= qbeg and end <= qend:
            return segt[ind]

        if (beg < qbeg and end <= qbeg) or (beg >= qend and end > qend):
            return 0

        mid = beg + (end - beg) // 2

        return _getsum(segt, beg, mid, qbeg, qend, 2*ind + 1) + \
            _getsum(segt, mid, end, qbeg, qend, 2*ind + 2)

    return _getsum(segt, 0, len(arr), qbeg, qend, 0)

def getsegt(arr):
    """
    :returns: segment tree
    """

    from math import log, ceil

    n = len(arr)
    segt = [0] * (2*(pow(2, int(ceil(log(n, 2))))) - 1)

    def _getsegt(arr, lo, hi, ind, segt):
        """
        :returns: current segment tree root value.
        """
        if hi - lo == 1:
            segt[ind] = arr[lo]
            return segt[ind]
        if lo >= hi:
            return 0
        mid = lo + (hi - lo) // 2
        segt[ind] = \
            _getsegt(arr, lo, mid, ind*2 + 1, segt) + \
            _getsegt(arr, mid, hi, ind*2 + 2, segt)
        return segt[ind]

    _getsegt(arr, 0, n, 0, segt)  # a populator...

    return segt


def do_tests():
    """
    :returns: None
    """
    pass

if __name__ == '__main__':
    do_tests()
