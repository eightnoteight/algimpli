#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin,star-args
from math import sqrt
import random


def pick_centroid(points):
    n = len(points)
    median = []
    for x in zip(*points):
        median.append(sum(x) // n)
    return tuple(median)


def dist(p1, p2):
    return sqrt(sum((d1i - d2i)**2 for d1i, d2i in zip(p1, p2)))


def assign_points_to_centinals(points, centinals):
    clusters = {}
    for p in points:
        assignment = min((dist(p, cent), cent) for cent in centinals)
        try:
            clusters[assignment[1]].append(p)
        except KeyError:
            clusters[assignment[1]] = [p]
    return clusters


def make_new_centinals(clusters):
    centinals = []
    for clust in clusters:
        centinals.append(pick_centroid(clusters[clust]))
    return tuple(centinals)


def kmeans(points, k, w, h):
    centinals = tuple(sorted(points[:k]))
    while True:
        clusters = assign_points_to_centinals(points, centinals)
        puts_2board(clusters, w, h)
        new_centinals = tuple(sorted(make_new_centinals(clusters)))
        if new_centinals == centinals:
            return clusters
        centinals = new_centinals

# ★☯♚♞♛♜♝♠♣♥♦✹▴▾▸◂❂☉
def puts_2board(clusters, w, h):
    board = [[' ']*w for _ in range(h)]
    symbols = u'◉' + '☿♁♅♆☢✧✩✰♢♡♧♤♔⚘✡▵▿▹◃♕♖♗♘❈♪⋆°♙✢✭✴✶'*5
    for i, clust in enumerate(sorted(clusters.keys())):
        for p in clusters[clust]:
            board[p[0]][p[1]] = symbols[i + 1]
        board[clust[0]][clust[1]] = symbols[0]
    for x in range(h):
        board[x] = ''.join(board[x])
    print('='*w)
    print('\n'.join(board))
    print('='*w)


def main():
    w, h = 200, 40
    points = list(zip(
        (random.randrange(h) for _ in range(1000)),
        (random.randrange(w) for _ in range(1000))))
    clusters = kmeans(points, int(input('clusters? ')), w, h)
    with open('clusters.info.out', 'w') as f:
        f.write(str(clusters))


main()
