#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin,star-args
from math import sqrt
import curses
from curses import wrapper as cursesWrapper
import random
from time import sleep


def pick_centroid(points):
    n = len(points)
    median = []
    for x in zip(*points):
        median.append(sum(x) // n)
    return tuple(median)


def dist(p1, p2):
    return sqrt(sum((d1i - d2i)**2 for d1i, d2i in zip(p1, p2)))


def assign_points_to_centinals(points, centinals):
    clusters = {}
    for p in points:
        assignment = min((dist(p, cent), cent) for cent in centinals)
        try:
            clusters[assignment[1]].append(p)
        except KeyError:
            clusters[assignment[1]] = [p]
    return clusters


def make_new_centinals(clusters):
    centinals = []
    for clust in clusters:
        centinals.append(pick_centroid(clusters[clust]))
    return tuple(centinals)


def kmeans(points, k, w, h, stdscr):
    centinals = tuple(sorted(points[:k]))
    while True:
        clusters = assign_points_to_centinals(points, centinals)
        puts_2board(clusters, w, h, stdscr)
        new_centinals = tuple(sorted(make_new_centinals(clusters)))
        if new_centinals == centinals:
            return clusters
        centinals = new_centinals
        sleep(0.5)

def puts_2board(clusters, w, h, stdscr):
    board = [[' ']*w for _ in range(h)]
    symbols = u'◉✧✩✰♢♡♧♤♔⚘✡▵▿▹◃♕♖♗♘♚♞♛♜♝♠♣♥♦✢✭✴✶❂✹❈♪⋆°♙▴▾▸◂'*5
    for i, clust in enumerate(clusters):
        for p in clusters[clust]:
            board[p[0]][p[1]] = symbols[i + 1]
        board[clust[0]][clust[1]] = symbols[0]
    for x in range(h):
        board[x] = ''.join(board[x])
    stdscr.addstr(0, 0, '='*w)
    stdscr.addstr(1, 0, '\n'.join(board))
    stdscr.addstr(h + 1, 0, '='*w)
    stdscr.refresh()


def main(stdscr):
    while True:
        stdscr.clear()
        w, h = 200, 40
        points = list(zip(
            (random.randrange(h) for _ in range(1000)),
            (random.randrange(w) for _ in range(1000))))
        curses.echo()
        stdscr.addstr(h + 2, 0, 'number of cluseters?(ex: 02, 09, 29, 99 etc,.) ')
        number_of_clusters = int(stdscr.getstr(h + 2, 48, 2))
        curses.noecho()
        clusters = kmeans(points, number_of_clusters, w, h, stdscr)
        with open('clusters.info.out', 'w') as f:
            f.write(str(clusters))

        stdscr.addstr(h + 2, 0, 'wanna try another one? (y/n) {}'.format(' '*50))
        curses.echo()
        # raise BaseException(stdscr.getch(h + 2, 29))
        if stdscr.getch(h + 2, 29) == ord('n'):
            break
        curses.noecho()


cursesWrapper(main)
