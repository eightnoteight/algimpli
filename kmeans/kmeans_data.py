#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin,star-args
from math import sqrt
import random


def pick_centroid(points):
    n = len(points)
    median = []
    for x in zip(*points):
        median.append(sum(x) // n)
    return tuple(median)


def dist(p1, p2):
    return sqrt(sum((d1i - d2i)**2 for d1i, d2i in zip(p1, p2)))


def assign_points_to_centinals(points, centinals):
    clusters = {}
    for p in points:
        assignment = min((dist(p, cent), cent) for cent in centinals)
        try:
            clusters[assignment[1]].append(p)
        except KeyError:
            clusters[assignment[1]] = [p]
    return clusters


def make_new_centinals(clusters):
    centinals = []
    for clust in clusters:
        centinals.append(pick_centroid(clusters[clust]))
    return tuple(centinals)


def kmeans(points, k):
    centinals = tuple(sorted(points[:k]))
    i = 1
    while True:
        clusters = assign_points_to_centinals(points, centinals)
        new_centinals = tuple(sorted(make_new_centinals(clusters)))
        print('iteration {:3d}: old centinals = {}'.format(i, centinals))
        print('iteration {:3d}: new centinals = {}'.format(i, new_centinals))
        if new_centinals == centinals:
            return clusters
        centinals = new_centinals
        i += 1

def main():
    psize = int(input('number of points? '))
    points = [tuple(map(int, input().split())) for _ in range(psize)]
    clusters = kmeans(points, int(input('clusters? ')))
    with open('clusters.info.out', 'w') as f:
        f.write(str(clusters))


main()
