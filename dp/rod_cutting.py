#!/usr/bin/env python
# encoding: utf-8
# pylint: disable=C0111
"""
Rod Cutting or exact Knapsack problem:
``````````````````````````````````````
parameters:
    arr:-
        is an array of tuples (length, price).
    rod:-
        is the length of the rod that you want to cut.
DP:
                            ------------  mprofit[item - 1][length]
    mprofit[item][length] =
                            ------------  max(
                                    mprofit[item - 1][length],
                                    mprofit[item][leng - arr[item - 1][0]] +
                                    arr[item - 1][1]))
return:
    maxprofit:-
        in cutting the rod optimally.
        but beware that there might be some waste(useless rod piece).
"""


def cut(arr, rod):
    mprofit = [[0]*(rod + 1) for _ in xrange(len(arr) + 1)]

    for item in xrange(1, len(arr) + 1):
        for leng in xrange(1, rod + 1):
            if arr[item - 1][0] > leng:
                mprofit[item][leng] = mprofit[item - 1][leng]
            else:
                mprofit[item][leng] = max(
                    mprofit[item - 1][leng],  # not chosing xth element
                    mprofit[item][leng - arr[item - 1][0]] +
                    arr[item - 1][1])

    return mprofit[-1][-1]


def tests():
    print cut(
        zip(
            [2, 4],
            [2, 8]), 5)


tests()
