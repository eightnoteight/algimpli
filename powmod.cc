#include <bits/stdc++.h>
using namespace std;

int64_t powmod(int64_t x, int64_t a, int64_t mod) {
    int64_t y = 1;
    while(a) {
        if (a & 1)
            y = (y * x) % mod;
        x = (x * x) % mod;
        a >>= 1;
    }
    return y;
}

int main() {
    cin >> testcases;
    for (int test = 1; test <= testcases; ++test) {
        // Code
    }
    return 0;
}
