#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,bad-builtin,missing-docstring
'''input
'''
from bisect import bisect_right, bisect_left
from random import random
import cProfile
import pstats
import StringIO

ceil_bisect = bisect_left


def floor_bisect(arr, val):
    return bisect_right(arr, val) - 1


def main():
    arr = [random() for _ in xrange(10**6)]
    prof = cProfile.Profile()
    stream = StringIO.StringIO()
    # print 'floor_bisect as definition'
    prof.enable()
    # -----------Profile #1 - Start--------------
    for val in [random() for _ in xrange(10**6)]:
        floor_bisect(arr, val)
    # -----------Profile #1 - End----------------

    prof.disable()
    pstats.Stats(prof, stream=stream).sort_stats('cumulative').print_stats()
    prof = cProfile.Profile()
    prof.enable()

    # -----------Profile #2 - Start--------------
    fn = lambda arr, x: bisect_right(arr, val) - 1
    for val in [random() for _ in xrange(10**6)]:
        fn(arr, val)
    # -----------Profile #2 - End----------------

    prof.disable()
    pstats.Stats(prof, stream=stream).sort_stats('cumulative').print_stats()
    prof = cProfile.Profile()
    prof.enable()

    # -----------Profile #3 - Start--------------
    for val in [random() for _ in xrange(10**6)]:
        bisect_right(arr, val)
    # -----------Profile #3 - End----------------

    prof.disable()
    pstats.Stats(prof, stream=stream).sort_stats('cumulative').print_stats()
    prof = cProfile.Profile()
    prof.enable()

    # -----------Profile #4 - Start--------------
    for val in [random() for _ in xrange(10**6)]:
        ceil_bisect(arr, val)
    # -----------Profile #4 - End----------------

    prof.disable()
    pstats.Stats(prof, stream=stream).sort_stats('cumulative').print_stats()
    prof = cProfile.Profile()
    prof.enable()

    # -----------Profile #5 - Start--------------
    for val in [random() for _ in xrange(10**6)]:
        bisect_left(arr, val)
    # -----------Profile #5 - End----------------

    prof.disable()
    pstats.Stats(prof, stream=stream).sort_stats('cumulative').print_stats()

    print stream.getvalue()


main()
