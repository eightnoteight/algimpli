class binsearchtree:

    def serialize(self):
        r = self.root
        stack = [r]
        result = []
        while stack:
            node = stack.pop()
            result.append(node)
            if not node.left:
                stack.append(node.left)
            if not node.right:
                stack.append(node.right)
        return '|'.join([str(x) for x in result])

    def deserialize(self, data):
        data = map(int, data.split('|'))
        for x in data:
            self.insert(x)


class bintree:

    def serialize(self):
        pass

    def deserialize(self):
        pass
