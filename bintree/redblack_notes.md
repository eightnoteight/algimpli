Red Black Trees
===============
-------------------------------------------------------------

Properties
----------
1. Every node is either a **red** or **black**
2. The **root** is **black**
3. Every leaf (None) is black
4. If a node is **red** then both its children are **black**
5. For each node, all simple paths from node to
   descendent leaves contain the same number of
   black nodes.
   
Excellent Exercises
---------------------
- **CLRS 13.1-3**
    - *Question*: Let us define a **relaxed binary tree** as a binary search
      tree that satisfies the red-black tree properties 1, 3, 4, 5.
      In other words, the root may be either red or black.
      Consider a red-black tree T whose root is red. If color the
      root of the tree as black and make no other changes to T,
      is the resulting tree a red-black tree ?
    - *Answer*: root is previously red, so both of it's children are black,
      and as the T satisfies 1, 3, 4, 5 properties we can say that children
      are real red-black trees. so it doesn't matter if we change the color
      of relaxed binary tree.
  

