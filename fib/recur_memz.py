memz = {}

def fib_rec_memz(n):
    try:
        return memz[n]
    except KeyError:
        pass
    if n < 2:
        memz[n] = n
        return memz[n]
    try:
        a = memz[n - 1]
    except KeyError:
        memz[n - 1] = fib_rec_memz(n - 1)
        a = memz[n - 1]
    try:
        b = memz[n - 2]
    except KeyError:
        memz[n - 2] = fib_rec_memz(n - 2)
        b = memz[n - 2]
    memz[n] = a + b
    return memz[n]

modmemz = {}
def fib_rec_memz_withmod(n, mod=1000000007):
    try:
        return modmemz[n] % mod
    except KeyError:
        pass
    if n < 2:
        modmemz[n] = n % mod
        return modmemz[n] % mod
    try:
        a = modmemz[n - 1] % mod
    except KeyError:
        modmemz[n - 1] = fib_rec_memz(n - 1) % mod
        a = modmemz[n - 1] % mod
    try:
        b = modmemz[n - 2] % mod
    except KeyError:
        modmemz[n - 2] = fib_rec_memz(n - 2) % mod
        b = modmemz[n - 2] % mod
    modmemz[n] = (a + b) % mod
    return modmemz[n] % mod
