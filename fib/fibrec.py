def fibrec(n):
    if n < 2:
        return n
    return fibrec(n - 1) + fibrec(n - 2)

def fibrecmod(n, mod=1000000007):
    if n < 2:
        return n % mod
    return (fibrecmod(n - 1) + fibrecmod(n - 2)) % mod
