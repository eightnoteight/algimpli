#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111
from random import randrange

# GleanStart
# 5 2 11 4 1 9 8 8 6 10
# GleanEnd

def _partition(arr, lo, hi, pivot):
    p = arr[pivot]
    arr[hi - 1], arr[pivot] = arr[pivot], arr[hi - 1]
    i = lo - 1
    for j in xrange(lo, hi):
        if arr[j] <= p:
            i += 1
            arr[i], arr[j] = arr[j], arr[i]
    return i

def _select(arr, lo, hi, spos):
    assert lo <= spos < hi
    if hi - lo == 1 and lo == spos:
        return lo
    while True:
        pos = _partition(arr, lo, hi, randrange(lo, hi))
        if pos == spos:
            return pos
        elif pos < spos:
            lo = pos + 1
        else:
            hi = pos

def median(arr):
    n = len(arr)
    return _select(arr, 0, n, (n - 1) // 2)


if __name__ == '__main__':
    arri = map(int, raw_input().split())
    print median(arri)
    print arri
