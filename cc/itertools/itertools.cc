#include <bits/stdc++.h>
using namespace std;

template<class REPT>
class _repeat : public std::iterator<>
{
    private:
        size_t _size;
        REPT value;
    public:
        _repeat(REPT init_val, size_t n) {
            _size = n;
            value = init_val;
        }
        ~_repeat();
};
