#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111
'''input
3
3 2
1 2 5
2 3 7
1 3
3 3
1 2 4
1 3 7
2 3 1
1 3
3 1
1 2 4
1 3
'''
from heapq import heappush, heappop


def dijkstra(G, v, e, src, dst):
    dist = [float('inf')]*v
    dist[src] = 0
    heap = [(dist[src], src)]
    while heap:
        node_dist, node = heappop(heap)
        for child, child_dist in G[node]:
            if node_dist + child_dist < dist[child]:
                dist[child] = node_dist + child_dist
                heappush(heap, (node_dist + child_dist, child))

    if dist[dst] == float('inf'):
        return 'NO'
    else:
        return dist[dst]


def main():
    for _ in xrange(int(raw_input())):
        v, e = map(int, raw_input().split())
        G = [[] for _ in xrange(v)]
        for _ in xrange(e):
            a, b, w = map(int, raw_input().split())
            G[a - 1].append((b - 1, w))
        src, dst = map(int, raw_input().split())
        print dijkstra(G, v, e, src - 1, dst - 1)


main()
