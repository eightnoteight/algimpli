#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def hamiltonc(graph, v):
    path = [0]
    visited = set([0])

    def feasible(j):
        return graph[path[-1]][j] and j not in visited

    def util(i):
        if i == v:
            print path
            return graph[path[-1]][path[0]]
        for j in xrange(v):
            if feasible(j):
                visited.add(j)
                path.append(j)
                if util(i + 1):
                    pass  # for all solutions
                    # return True  # for one solution.
                path.pop()
                visited.remove(j)
    util(1)

def main():
    graph = [
        [0, 1, 0, 1, 0],
        [1, 0, 1, 1, 1],
        [0, 1, 0, 0, 1],
        [1, 1, 0, 0, 1],
        [0, 1, 1, 1, 0]
    ]
    hamiltonc(graph, 5)

main()
