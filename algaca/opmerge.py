#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from heapq import heapify, heappop, heappush

def opmerge(arr):
    """
    arr = [(id, occur), ...]
    node = (occur, value, left, right)
    """
    pqueue = [(x[1], x[0], None, None) for x in arr]
    heapify(pqueue)
    while len(pqueue) > 1:
        tree1 = heappop(pqueue)
        tree2 = heappop(pqueue)
        heappush(pqueue, (tree1[0] + tree2[0], None, tree1, tree2))
    return heappop(pqueue)

def main():
    print opmerge([(1, 20), (2, 30), (3, 40)])

main()
