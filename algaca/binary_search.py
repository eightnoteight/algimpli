#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def bsearch(arr, elem):
    l, h = 0, len(arr)
    while l < h:
        m = l + (h - l) // 2
        if arr[m] > elem:
            h = m
        elif arr[m] < elem:
            l = m + 1
        else:
            return m
    return -1

print bsearch(map(int, raw_input().split()), int(raw_input()))
