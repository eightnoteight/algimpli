#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def knapsack(arr, mw):
    """
    ::param::
        :arr: [(cost, weight)]
    """
    arr = sorted(arr, key=lambda x: x[0] / x[1], reverse=True)
    kp = []
    mp = 0
    for c, w in arr:
        if mw >= w:
            kp.append((c, w, 1))
            mw -= w
            mp += c
        else:
            fraction = mw / w
            kp.append((c, w, fraction))
            mw -= w*fraction
            mp += c*fraction
    return mp, kp

def main():
    print knapsack(
        zip(
            map(float, raw_input().split()),
            map(float, raw_input().split())),
        float(raw_input()))

main()
