#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
def jobshed(arr):
    """
    ::param::
        :arr: [(profit, deadline), ...]
    """
    arr, jobs = sorted(arr, reverse=True), []
    for x in xrange(len(arr)):
        if arr[x][1] >= len(jobs) + 1:
            jobs.append(arr[x])
    return sum([x[0] for x in jobs]), jobs


print jobshed([tuple(map(int, raw_input().split())) for _ in xrange(int(raw_input()))])
