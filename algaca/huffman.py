#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from heapq import heappush, heappop, heapify

def huffman(arr):
    """
    arr = [(id, occur), ...]
    node = (occur, value, left, right)
    """
    def decode(curnode, val):
        if curnode is None:
            return
        if curnode[1] is not None:
            huff[curnode[1]] = ''.join(val)
            return
        val.append('1')
        decode(curnode[2], val)
        val.pop()
        val.append('0')
        decode(curnode[3], val)
        val.pop()

    pqueue = [(x[1], x[0], None, None) for x in arr]
    heapify(pqueue)
    huff = {x[0]: bytearray() for x in arr}
    while len(pqueue) > 1:
        tree1 = heappop(pqueue)
        tree2 = heappop(pqueue)
        heappush(pqueue, (tree1[0] + tree2[0], None, tree1, tree2))
    tree = heappop(pqueue)
    decode(tree, [])
    return huff

def main():
    print huffman([(1, 12), (9, 20), (2, 1)])

main()
