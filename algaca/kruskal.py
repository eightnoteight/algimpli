#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
import array

class unionfind(object):
    def __init__(self, n):
        self._length = n
        self._roots = array.array('i', [x for x in xrange(n)])
        self._weights = array.array('i', [1]*n)

    def __str__(self):
        return str(self._roots)

    def union(self, a, b):
        aroot = self.find(a)
        broot = self.find(b)
        if self._weights[aroot] > self._weights[broot]:
            self._roots[broot] = aroot
            self._weights[aroot] += self._weights[broot]
            self._weights[broot] = 0
        else:
            self._roots[aroot] = broot
            self._weights[broot] += self._weights[aroot]
            self._weights[aroot] = 0

    def connected(self, a, b):
        return self.find(a) == self.find(b)

    def find(self, a):
        while self._roots[a] != a:
            self._roots[a] = self._roots[self._roots[a]]
            a = self._roots[a]
        return a


def kruskal(Ge, n):
    # Ge is a list of edges.
    # n is the number of vertices.
    Ge.sort(key=lambda x: x[2])
    res = 0
    uf = unionfind(n)
    tree = []
    for _, edge in enumerate(Ge):
        if not uf.connected(edge[0], edge[1]):
            res += edge[2]
            uf.union(edge[0], edge[1])
            tree.append(edge)
    return res, tree

def main():
    Ge = []
    v, e = map(int, raw_input().split())
    for _ in xrange(e):
        Ge.append(tuple(map(int, raw_input().split())))
    print kruskal(Ge, v)

main()
