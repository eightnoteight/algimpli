#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def knapsack(arr, W):
    """
    ::param::
        :arr: [(profit, weight)]
    """
    contents = []
    sol = [0, ()]
    def util(i, cp, cw):
        if cw > W:
            return
        if i == len(arr):
            if sol[0] < cp:
                sol[0] = cp
                sol[1] = tuple(contents)
            return
        contents.append(1)
        util(i + 1, cp + arr[i][0], cw + arr[i][1])
        contents.pop()
        contents.append(0)
        util(i + 1, cp, cw)
        contents.pop()
    util(0, 0, 0)
    print sol

knapsack([tuple(map(int, raw_input().split())) for _ in xrange(int(raw_input()))], int(raw_input()))
