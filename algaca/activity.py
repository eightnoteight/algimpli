#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
def activity_selection(arr):
    """
    ::param::
        :arr: [(start, end), ...]
    """
    arr = sorted(arr, key=lambda x: x[1])
    activities = [arr[0]]
    for s, e in arr:
        if s > activities[-1][1]:
            activities.append((s, e))
    return activities


print activity_selection([
    tuple(map(int, raw_input().split())) for _ in xrange(int(raw_input()))])
