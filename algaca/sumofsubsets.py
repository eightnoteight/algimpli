#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def subsetsum(arr, s):
    sol = []
    def util(i, cs):
        if i == len(arr):
            if cs == s:
                print sol
            return cs == s
        sol.append(1)
        if util(i + 1, cs + arr[i]):
            pass  # for all solutions
            # return True  # for one solution.
        sol.pop()
        sol.append(0)
        if util(i + 1, cs):
            pass  # for all solutions
            # return True # for one solution.
        sol.pop()

    return util(0, 0)

def main():
    subsetsum(map(int, raw_input().split()), int(raw_input()))

main()
