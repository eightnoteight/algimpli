#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def knapsack(arr, W):
    """
    ::param::
        :arr: [(profit, weight)]
    """
    costs = [x[0] for x in arr]
    weights = [x[1] for x in arr]
    n = len(weights)
    ks = [[0]*(W + 1) for _ in xrange(n + 1)]
    for i in xrange(1, n + 1):
        for j in xrange(1, W + 1):
            if weights[i - 1] > j:
                ks[i][j] = ks[i - 1][j]
            else:
                ks[i][j] = max(ks[i - 1][j], ks[i - 1][j - weights[i - 1]] + costs[i - 1])
    return ks[n][W]

print knapsack(
    [
        tuple(map(int, raw_input().split())) for _ in xrange(int(raw_input()))],
    int(raw_input()))
