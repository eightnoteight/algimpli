#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def add(lhs, rhs, base):
    length = max(len(lhs), len(rhs))
    lhs = [0 for i in range(len(lhs), length)] + lhs
    rhs = [0 for i in range(len(rhs), length)] + rhs
    carry = 0
    result = []
    for i in range(1, len(lhs) + 1):
        column = lhs[-i] + rhs[-i] + carry
        result.append(column % base)
        carry = column / base
    if carry != 0:
        result.append(carry)
    result.reverse()
    return result

def subtract(lhs, rhs, base):
    length = max(len(lhs), len(rhs))
    lhs = [0 for i in range(len(lhs), length)] + lhs
    rhs = [0 for i in range(len(rhs), length)] + rhs
    result = []
    for i in range(1, len(lhs) + 1):
        difference = lhs[-i] - rhs[-i]
        if difference >= 0:
            result.append(difference)
        else:
            j = i + 1
            while j <= length:
                lhs[-j] = (lhs[-j] + (base - 1)) % base
                if lhs[-j] != base - 1:
                    break
                else:
                    j = j + 1
            result.append(difference + base)
    result.reverse()
    return result

def multiply(lhs, rhs, base):
    length = max(len(lhs), len(rhs))
    lhs = [0]*(length - len(lhs)) + lhs
    rhs = [0]*(length - len(rhs)) + rhs
    if length == 1:
        result = lhs[0] * rhs[0]
        return [result] if result < base else [result / base, result % base]
    m0 = (length + 1) / 2
    m1 = length / 2
    x0 = lhs[: m0]
    x1 = lhs[m0: ]
    y0 = rhs[: m0]
    y1 = rhs[m0: ]
    p0 = multiply(x0, y0, base)
    p1 = multiply(add(x0, x1, base), add(y0, y1, base), base)
    p2 = multiply(x1, y1, base)
    z0 = p0
    z1 = subtract(p1, add(p0, p2, base), base)
    z2 = p2
    z0prod = z0 + [0]*(2*m1)
    z1prod = z1 + [0]*m1
    z2prod = z2
    return add(add(z0prod, z1prod, base), z2prod, base)

def main():
    print ''.join(
        map(str, multiply(
            map(int, raw_input()),
            map(int, raw_input()),
            int(raw_input())))).strip('0')

main()
