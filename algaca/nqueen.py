#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def nqueen(n):
    def feasible(i, j):
        for x in xrange(n):
            if board[i][x] or board[x][j]:
                return False
        for x, y in zip(xrange(i - 1, -1, -1), xrange(j - 1, -1, -1)):
            if board[x][y]:
                return False
        for x, y in zip(xrange(i - 1, -1, -1), xrange(j + 1, n)):
            if board[x][y]:
                return False
        return True

    def put_queens(i):
        if i == n:
            print "Solution:"
            for x in xrange(n):
                print "\t", board[x]
            return True
        for j in xrange(n):
            if feasible(i, j):
                board[i][j] = 1
                if put_queens(i + 1):
                    # pass  # for all solutions.
                    return True  # for one solutions
                board[i][j] = 0  # backtracking
        return False

    board = [[0]*n for _ in xrange(n)]
    put_queens(0)

def main():
    nqueen(8)

main()
