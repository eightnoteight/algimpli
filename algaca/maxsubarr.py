#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def merge(arr, lo, hi, mid):
    ml, mlpos, i = arr[mid - 1], mid - 1, mid - 2
    mr, mrpos, j = arr[mid], mid + 1, mid + 1
    cs = arr[mid - 1]
    while i >= lo:
        cs += arr[i]
        if cs > ml:
            ml, mlpos = cs, i
        i -= 1
    cs = arr[mid]
    while j < hi:
        cs += arr[j]
        if cs > mr:
            mr, mrpos = cs, j + 1
        j += 1
    return ml + mr, (mlpos, mrpos)

def maxsubarr(arr, lo, hi):
    if hi - lo <= 1:
        return sum(arr[lo: hi]), (lo, hi)
    mid = lo + ((hi - lo) // 2)
    left, lp = maxsubarr(arr, lo, mid)
    right, rp = maxsubarr(arr, mid, hi)
    overall, op = merge(arr, lo, hi, mid)
    return max((left, lp), (right, rp), (overall, op))

def main():
    arr = map(int, raw_input().split())
    ms, (lo, hi) = maxsubarr(arr, 0, len(arr))
    print ms, arr[lo: hi]

main()
