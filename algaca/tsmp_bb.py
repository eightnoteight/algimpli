#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def tsmp(graph):
    mcost = [float('inf'), tuple()]
    cities = []
    visited = set()
    def branch(i, cs):
        if i == len(graph):
            if cs < mcost[0]:
                mcost[0] = cs
                mcost[1] = tuple(cities)
            return True
        for j in xrange(len(graph)):
            if graph[cities[-1]][j] is not float('inf') and j not in visited:
                cities.append(j)
                visited.add(j)
                if branch(i + 1, cs + graph[cities[-1]][j]):
                    return True  # bound
                visited.remove(j)
                cities.pop()
    for x in xrange(len(graph)):
        visited.add(x)
        cities.append(x)
        branch(x, 0)
        cities.pop()
        visited.remove(x)
    print mcost

def main():
    tsmp([map(eval, raw_input().split()) for _ in xrange(int(raw_input()))])

main()
