#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
from math import ceil, log

def read():
    lines = stdin.read().splitlines()
    A = []
    B = []
    matrix = A
    for line in lines:
        if line != "":
            matrix.append(map(int, line.split()))
        else:
            matrix = B
    return A, B

def printMatrix(matrix):
    for line in matrix:
        print "\t".join(map(str,line))

def ikjMatrixProduct(A, B):
    n = len(A)
    C = [[0 for i in xrange(n)] for j in xrange(n)]
    for i in xrange(n):
        for k in xrange(n):
            for j in xrange(n):
                C[i][j] += A[i][k] * B[k][j]
    return C

def add(A, B):
    n = len(A)
    C = [[0 for j in xrange(0, n)] for i in xrange(0, n)]
    for i in xrange(0, n):
        for j in xrange(0, n):
            C[i][j] = A[i][j] + B[i][j]
    return C

def subtract(A, B):
    n = len(A)
    C = [[0 for j in xrange(0, n)] for i in xrange(0, n)]
    for i in xrange(0, n):
        for j in xrange(0, n):
            C[i][j] = A[i][j] - B[i][j]
    return C

def strassenR(A, B):
    n = len(A)

    if n <= LEAF_SIZE:
        return ikjMatrixProduct(A, B)
    newSize = n/2
    a11 = [[0]*newSize for _ in xrange(newSize)]
    a12 = [[0]*newSize for _ in xrange(newSize)]
    a21 = [[0]*newSize for _ in xrange(newSize)]
    a22 = [[0]*newSize for _ in xrange(newSize)]

    b11 = [[0]*newSize for _ in xrange(newSize)]
    b12 = [[0]*newSize for _ in xrange(newSize)]
    b21 = [[0]*newSize for _ in xrange(newSize)]
    b22 = [[0]*newSize for _ in xrange(newSize)]

    aResult = [[0]*newSize for _ in xrange(newSize)]
    bResult = [[0]*newSize for _ in xrange(newSize)]


    for i in xrange(newSize):
        for j in xrange(newSize):
            a11[i][j] = A[i][j]
            a12[i][j] = A[i][j + newSize]
            a21[i][j] = A[i + newSize][j]
            a22[i][j] = A[i + newSize][j + newSize]

            b11[i][j] = B[i][j]
            b12[i][j] = B[i][j + newSize]
            b21[i][j] = B[i + newSize][j]
            b22[i][j] = B[i + newSize][j + newSize]


    aResult = add(a11, a22)
    bResult = add(b11, b22)
    p1 = strassenR(aResult, bResult)

    aResult = add(a21, a22)
    p2 = strassenR(aResult, b11)

    bResult = subtract(b12, b22)
    p3 = strassenR(a11, bResult)

    bResult = subtract(b21, b11)
    p4 = strassenR(a22, bResult)

    aResult = add(a11, a12)
    p5 = strassenR(aResult, b22)

    aResult = subtract(a21, a11)
    bResult = add(b11, b12)
    p6 = strassenR(aResult, bResult)

    aResult = subtract(a12, a22)
    bResult = add(b21, b22)
    p7 = strassenR(aResult, bResult)


    c12 = add(p3, p5)
    c21 = add(p2, p4)

    aResult = add(p1, p4)
    bResult = add(aResult, p7)
    c11 = subtract(bResult, p5)

    aResult = add(p1, p3)
    bResult = add(aResult, p6)
    c22 = subtract(bResult, p2)


    C = [[0]*n for _ in xrange(0, n)]
    for i in xrange(0, newSize):
        for j in xrange(0, newSize):
            C[i][j] = c11[i][j]
            C[i][j + newSize] = c12[i][j]
            C[i + newSize][j] = c21[i][j]
            C[i + newSize][j + newSize] = c22[i][j]
    return C

def strassen(A, B):
    assert type(A) == list and type(B) == list
    assert len(A) == len(A[0]) == len(B) == len(B[0])
    nextPowerOfTwo = lambda n: 2**int(ceil(log(n, 2)))
    n = len(A)
    m = nextPowerOfTwo(n)
    APrep = [[0]*m for _ in xrange(m)]
    BPrep = [[0]*m for _ in xrange(m)]
    for i in xrange(n):
        for j in xrange(n):
            APrep[i][j] = A[i][j]
            BPrep[i][j] = B[i][j]
    CPrep = strassenR(APrep, BPrep)
    C = [[0]*n for _ in xrange(n)]
    for i in xrange(n):
        for j in xrange(n):
            C[i][j] = CPrep[i][j]
    return C

if __name__ == "__main__":
    LEAF_SIZE = 8
    Amat, Bmat = read()
    Cmat = strassen(Amat, Bmat)
    printMatrix(Cmat)
