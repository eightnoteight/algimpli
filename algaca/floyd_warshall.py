#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from copy import deepcopy

def warshall(adjMat):
    n = len(adjMat)
    dp = deepcopy(adjMat)
    for k in xrange(n):
        for i in xrange(n):
            for j in xrange(n):
                if dp[i][k] + dp[k][j] < dp[i][j]:
                    dp[i][j] = dp[i][k] + dp[k][j]
    return dp

def main():
    print warshall([map(eval, raw_input().split()) for _ in xrange(int(raw_input()))])

main()
