#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
def partition(arr, lo, hi, pivot):
    arr[pivot], arr[lo] = arr[lo], arr[pivot]
    if hi - lo == 1:
        return lo
    i, j = lo, hi - 1
    while i <= j:
        if arr[j] < arr[lo] and arr[i] > arr[lo]:
            arr[i], arr[j] = arr[j], arr[i]
        if arr[i] <= arr[lo]:
            i += 1
        if arr[j] >= arr[lo]:
            j -= 1
    for x in xrange(max(i - 1, lo + 1), min(j + 2, hi)):
        if arr[x] >= arr[lo]:
            arr[lo], arr[x - 1] = arr[x - 1], arr[lo]
            return x - 1
    arr[lo], arr[hi - 1] = arr[hi - 1], arr[lo]
    return hi - 1

def quicksort(arr, lo, hi):
    if lo >= hi:
        return
    pos = partition(arr, lo, hi, lo)
    quicksort(arr, lo, pos)
    quicksort(arr, pos + 1, hi)

def main():
    arr = map(int, raw_input().split())
    quicksort(arr, 0, len(arr))
    print arr

main()
