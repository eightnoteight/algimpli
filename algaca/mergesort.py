#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
def merge(arr, lo, hi, mid):
    left = arr[lo: mid]
    left.append(float('inf'))
    right = arr[mid: hi]
    right.append(float('inf'))
    i, j = 0, 0
    for x in xrange(lo, hi):
        if left[i] < right[j]:
            arr[x] = left[i]
            i += 1
        else:
            arr[x] = right[j]
            j += 1

def mergesort(arr, lo, hi):
    if hi - lo <= 1:
        return
    mid = lo + ((hi - lo) // 2)
    mergesort(arr, lo, mid)
    mergesort(arr, mid, hi)
    merge(arr, lo, hi, mid)

def main():
    arr = map(int, raw_input().split())
    mergesort(arr, 0, len(arr))
    print arr

main()
