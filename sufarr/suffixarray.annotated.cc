#include <bits/stdc++.h>
#ifdef __mr__
    #include "prettyprint.hpp"
#endif
using namespace std;

vector<int32_t> getSuffixArray(const string& str) {
    // indices is the state of suffix array considering the 2**x characters
    // ranks stores the ranks of indices based the first 2**x characters
    // subranks stores the ranks of (index + 2**x) of indices based on the ranks
    // newranks is dummy memory for ranks
    // locations, is basically inverse of indices; i.e x = locations[y] if and only if y = indices[x]
    vector<int32_t> ranks(str.size()), subranks(str.size()),
        indices(str.size()), newranks(str.size()), locations(str.size());
    // intializing indices by considering first 0 characters
    for (size_t x = 0; x < str.size(); ++x)
        indices[x] = x;
    // arranging indices based on first 1 character.
    sort(indices.begin(), indices.end(), [&str](int32_t i, int32_t j) {
        return str[i] < str[j];
    });
    // assigning ranks for the present state of indices.
    // ranks are assigned such that if the considering characters are equal then they get same ranks.
    for (size_t x = 1; x < str.size(); ++x)
        ranks[x] = ranks[x - 1] + (str[indices[x]] != str[indices[x - 1]]);
    for (size_t x = 1; x <= str.size(); x <<= 1) {
        // calculating the inverse of indices
        for (size_t y = 0; y < str.size(); ++y)
            locations[indices[y]] = y;
        // calculating subranks based on ranks
        for (size_t y = 0; y < str.size(); ++y)
            subranks[y] = (indices[y] + x < str.size()) ? ranks[locations[indices[y] + x]] : -1;
        // updating the state of indices by considering the first 2*x characters of suffixes
        sort(indices.begin(), indices.end(), [&ranks, &subranks, &locations](int32_t i, int32_t j) {
            // calculating the predecessor based on first x characters of the two suffixes,
            // if the first x characters of the two suffixes are different
            if (ranks[locations[i]] != ranks[locations[j]])
                return ranks[locations[i]] < ranks[locations[j]];
            // if the first x characters of the two suffixes are same the calculate
            // considering the first 2*x characters a.k.a characters from x -> 2*x of the suffix
            return subranks[locations[i]] < subranks[locations[j]];
        });
        // calculate newranks(considering first 2*x characters of the suffix)
        // using the ranks(based 0 -> x characters of the suffix)
        // and subranks(based on x -> 2*x characters of the suffix)
        for (size_t y = 1; y < str.size(); ++y)
            // newranks are calculated such that if the considering characters(0 -> 2*x) of the suffix
            // are equal then they get same rank.
            newranks[y] = newranks[y - 1] + (
                ranks[locations[indices[y]]] != ranks[locations[indices[y - 1]]] ||
                subranks[locations[indices[y]]] != subranks[locations[indices[y - 1]]]);
        // copy the newranks into ranks
        copy(newranks.begin(), newranks.end(), ranks.begin());
    }
    return move(indices);
}

int main() {
    string str;
    vector<int32_t> prtemp;
    cin >> str;
    prtemp = getSuffixArray(str);
    cout << prtemp << endl;
    return 0;
}
