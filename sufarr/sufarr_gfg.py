

def sufarr(txt):
    n = len(txt)
    class suffx(object):
        def __init__(self):
            self.index = 0
            self.rank = [0, 0]

    def sufcmp(a, b):
        return (a.rank[1] < b.rank[1]) if (a.rank[0] == b.rank[0]) else (a.rank[0] < b.rank[0])
    suffixes = [suffx() for x in xrange(n)]
    for x in xrange(n):
        suffixes[x].index = x
        suffixes[x].rank[0] = ord(txt[x]) - ord('a')
        suffixes[x].rank[1] = (ord(txt[x + 1]) - ord('a')) if (x + 1) < n else -1
    suffixes.sort(cmp=sufcmp)
    ind = [0] * n
    k = 4
    while k < 2 * n:
        rank = 0
        prev_rank = suffixes[0].rank[0]
        suffixes[0].rank[0] = rank
        ind[suffixes[0].index] = 0
        for i in xrange(1, n):
            if suffixes[i].rank[0] == prev_rank and suffixes[i].rank[1] == suffixes[i - 1].rank[1]:
                prev_rank = suffixes[i].rank[0]
                suffixes[i].rank[0] = rank
            else:
                prev_rank = suffixes[i].rank[0]
                suffixes[i].rank[0] = rank + 1
                rank += 1
            ind[suffixes[i].index] = i

        for i in xrange(n):
            nextindex = suffixes[i].index + k // 2
            suffixes[i].rank[1] = (suffixes[ind[nextindex]].rank[0]) if (nextindex < n) else -1

        suffixes.sort(cmp=sufcmp)
        k *= 2
    sa = [0] * n
    for i in xrange(n):
        sa[i] = suffixes[i].index

    return sa


if __name__ == '__main__':
    print sufarr('mississipi')
