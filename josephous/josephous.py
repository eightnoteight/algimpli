try:
    range = xrange
    input = raw_input
except:
    pass



def josephous(n, k):
    """
    O(n) algo, it is advised to use O(k*log(n))
    algo for large n and small k
    """
    ans = 1  # for josephous(1, k)
    for x in range(2, n + 1):
        ans = ((ans + k - 1) % x) + 1
    return ans
