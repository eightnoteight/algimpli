unsigned long long int
josephous(
        unsigned long long int n,
        unsigned long long int k)
{
    /*
     * O(n) algo, it is advised to use O(k*log(n))
     * algo for large n and small k
     * * * * * * * * * * * * * * * * * * * * * * * */
    unsigned long long int ans = 1;
    for(i = 2; i < n + 1; i++)
        ans = ((ans + k - 1) % i) + 1;
    return ans;
}
