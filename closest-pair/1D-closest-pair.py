#!/usr/bin/env python
# -*- encoding: utf-8 -*-

try:
    range = xrange
    input = raw_input
except:
    pass

def ClosestPair1D(points):
    """
    points is an array of integers....
    O(n*log(n)) algorithm
    """
    points.sort()  # O(n*log(n))
    md = float('inf')  # minimum distance
    for x in range(1, len(points)):
        if points[x] - points[x - 1] < md:
            md = points[x] - points[x - 1]  # O(n)

    return md  # O(n*log(n))
