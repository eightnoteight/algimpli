#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111
from heapq import heapify, heappush, heappop


class pqueue(object):
    def __init__(self, arr=[]):
        self.heap = arr[:]
        heapify(self.heap)

    def push(self, elem):
        heappush(self.heap, elem)

    def pop(self):
        return heappop(self.heap)

    def top(self):
        return self.heap[0]
