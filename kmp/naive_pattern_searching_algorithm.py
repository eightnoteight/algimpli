def search(pattern, source):
    m = len(pattern)
    n = len(source)
    matches = []
    for i in xrange(n):
        j = 0
        while i + j < n and j < m and pattern[j] == source[i + j]:
            j += 1
        if j == m:
            matches.append(i)
    return matches
