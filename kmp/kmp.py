def _kmp_failure(pattern):
    # THE KMP FAILURE FUNCTION
    pattern_length = len(pattern)
    failure = [0]*pattern_length
    i = 1
    j = 0
    while i < pattern_length:
        if pattern[i] == pattern[j]:
            failure[i] = j + 1
            i += 1
            j += 1
        elif j:
            j = failure[j - 1]
        else:
            failure[i] = 0
            i += 1

    return failure


def kmp(pattern, text):
    failure = _kmp_failure(pattern)
    matches = []
    text_length = len(text)
    pattern_length = len(pattern)
    i = 0
    j = 0
    while i < text_length:
        if pattern[j] == text[i]:
            j += 1
            i += 1

        if j == pattern_length:
            matches.append(i - j)
            j = failure[j - 1]
        elif pattern[j] != text[i]:
            if j:
                j = failure[j - 1]
            else:
                i += 1
    return matches
