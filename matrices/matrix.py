#!/usr/bin/env python
# -*- encoding: utf-8 -*-


class matrow(object):
    def __init__(self, arr, lo, hi):
        self.arr = arr
        self.lo = lo
        self.hi = hi

    def __getitem__(self, ind):
        if ind < 0:
            if self.hi - self.lo + ind <= 0:
                raise IndexError
            return self.arr[self.hi + ind]
        if ind >= self.hi - self.lo:
            raise IndexError
        return self.arr[self.lo + ind]

    def __setitem__(self, ind, val):
        if ind < 0:
            if self.hi - self.lo + ind <= 0:
                raise IndexError
            self.arr[self.hi + ind] = val
            return
        if ind >= self.hi - self.lo:
            raise IndexError
        self.arr[self.lo + ind] = val
        return

class matrix(object):

    def __init__(self, rows, cols):
        from array import array
        self.rows = rows
        self.cols = cols
        self.arr = array('l', [0]*rows*cols)
    def __getitem__(self, ind):
        if ind < 0:
            if self.rows + ind <= 0:
                raise IndexError
            ind = self.rows + ind
        if ind >= self.rows:
            raise IndexError
        return matrow(self.arr, ind*self.cols, (ind + 1)*self.cols)
