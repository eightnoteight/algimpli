#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: skip-file
import os
import mergesort_v1
import mergesort_v2
import insertionsort
import utils as sortutils
import cProfile

def main():
    # sort = mergesort_v2.sort
    sort = list.sort
    for testFile in sorted(os.listdir('tests'))[:5]:
        nums = map(int, open('tests/' + testFile, 'r').read().split())
        cProfile.runctx('sort(nums)', globals(), locals())
        if not sortutils.is_sorted(nums):
            print u'test #(%s) Failed  ➡  ⚠ ⚠ ⚠ ⚠ ⚠' % testFile
        else:
            print u'test #(%s) Passed  ➡  ★ ★ ★ ★ ★' % testFile

main()
