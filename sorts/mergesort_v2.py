#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from insertionsort import _isort as isort

def merge(arr, tstorage, lo, hi):
    tstorage[lo: hi] = arr[lo: hi]
    mid = lo + ((hi - lo) // 2)
    i, j = lo, mid
    for x in xrange(lo, hi):
        if i < mid and j < hi and tstorage[i] <= tstorage[j]:
            arr[x] = tstorage[i]
            i += 1
        elif i < mid and j < hi and tstorage[j] <= tstorage[i]:
            arr[x] = tstorage[j]
            j += 1
        elif i < mid:
            arr[x] = tstorage[i]
            i += 1
        elif j < hi:
            arr[x] = tstorage[j]
            j += 1
        else:
            raise RuntimeError

def _sort(arr, tstorage, lo, hi):
    if hi - lo <= 20:
        isort(arr, lo, hi)
        return
    mid = lo + ((hi - lo) // 2)
    _sort(arr, tstorage, lo, mid)
    _sort(arr, tstorage, mid, hi)
    merge(arr, tstorage, lo, hi)

def sort(arr):
    """
    Merge Sort V2:
        plain merge sort with insertion sort at the leaves.
    """
    tstorage = [None]*len(arr)
    _sort(arr, tstorage, 0, len(arr))
