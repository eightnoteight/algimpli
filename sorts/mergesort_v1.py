#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def merge(arr, tstorage, lo, hi):
    tstorage[lo: hi] = arr[lo: hi]
    mid = lo + ((hi - lo) // 2)
    i, j = lo, mid
    for x in xrange(lo, hi):
        if i < mid and j < hi and tstorage[i] <= tstorage[j]:
            arr[x] = tstorage[i]
            i += 1
        elif i < mid and j < hi and tstorage[j] <= tstorage[i]:
            arr[x] = tstorage[j]
            j += 1
        elif i < mid:
            arr[x] = tstorage[i]
            i += 1
        elif j < hi:
            arr[x] = tstorage[j]
            j += 1
        else:
            raise RuntimeError

def _sort(arr, tstorage, lo, hi):
    if hi - lo <= 1:
        return
    mid = lo + ((hi - lo) // 2)
    _sort(arr, tstorage, lo, mid)
    _sort(arr, tstorage, mid, hi)
    merge(arr, tstorage, lo, hi)

def sort(arr):
    """
    Merge Sort V1:
            plain merge sort.
    """
    tstorage = [None]*len(arr)
    _sort(arr, tstorage, 0, len(arr))
