#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def is_sorted(arr):
    last = float('-inf')
    for x in arr:
        if x < last:
            return False
        last = x
    return True

