#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def _isort(arr, lo, hi):
    for x in xrange(lo, hi):
        for y in xrange(x - 1, lo - 1, -1):
            if arr[y] > arr[y + 1]:
                arr[y], arr[y + 1] = arr[y + 1], arr[y]

def isort(arr):
    _isort(arr, 0, len(arr))

def main():
    # a sanity test
    import random
    import utils
    arr = [random.randint(0, 10**6) for _ in xrange(100)]
    isort(arr)
    print utils.is_sorted(arr)

if __name__ == '__main__':
    main()
