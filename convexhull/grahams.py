#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin,star-args
from functools import partial


def ccw(a, b, c):
    """
                    1   | ax  ay  1 |
    area(a, b, c) = - * | bx  by  1 |
                    2   | cx  cy  1 |
    a vector calculus application,
        counter-clockwise turn positive area.
        clockwise turn negative area.
        collinear zero area.
    """
    area = ((b[0] - a[0])*(c[1] - a[1]) - (b[1] - a[1])*(c[0] - a[0]))
    if area > 0:
        return 1
    elif area < 0:
        return -1
    return 0


def grahams(points):
    """
    :points:
        [(x1, y1), (x2, y2), (x3, y3), ..., (xn, yn)]
    """
    def polarcmp(a, b, c):
        ret = ccw(a, b, c)
        if ret:
            return ret
        dista = (a[0] - c[0])**2 + (a[1] - c[1])**2
        distb = (b[0] - c[0])**2 + (b[1] - c[1])**2
        if  dista < distb:
            return 1
        elif distb < dista:
            return -1
        return 0

    n = len(points)
    start = min([((y, x), i) for i, (x, y) in enumerate(points)])
    points[start[1]], points[-1] = points[-1], points[start[1]]
    start = points.pop()
    # think about the reverse carefully.
    points = sorted(points, cmp=partial(polarcmp, c=start))
    points.append(start)
    points = points[::-1]
    assert start == points[0]
    hull = []
    for x in xrange(n):
        while len(hull) >= 2 and ccw(hull[-2], hull[-1], points[x]) <= 0:
            hull.pop()
        hull.append(points[x])
    return hull


def main():
    from random import randrange
    points = [(randrange(20), randrange(10)) for _ in xrange(30)]
    hull = grahams(points)
    print 'original points'
    out = []
    for y in xrange(10, -1, -1):
        for x in xrange(20):
            out.append('*' if (x, y) in points else ' ')
        out.append('\n')
    print ''.join(out)
    print 'convex hull points'
    out = []
    for y in xrange(10, -1, -1):
        for x in xrange(20):
            out.append('*' if (x, y) in hull else ' ')
        out.append('\n')
    print ''.join(out)


def test():
    points = [
        (0, 0), (1, 0), (8, 0), (17, 0), (19, 1),
        (11, 1), (18, 2), (18, 2), (16, 2), (9, 2),
        (11, 3), (16, 5), (3, 1), (18, 7), (10, 4),
        (10, 4), (11, 5), (19, 9), (2, 1), (6, 3),
        (17, 9), (15, 8), (14, 8), (15, 9), (9, 6),
        (12, 8), (7, 5), (4, 3), (7, 7), (1, 4)]
    hull = grahams(points)
    print hull

# main()
test()