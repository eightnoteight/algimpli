#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
"""
description:
    Brute Force Algorithm for obtaining vertices of
    convexhull(of given points).
note:
    checking if the point is inside the triangle using
    Barycentric Coordinates.
"""
from itertools import combinations

def convexhull(points):
    def isPtInside(pt, x, y, z):
        try:
            alpha = ((y[1] - z[1])*(pt[0] - z[0]) + (z[0] - y[0])*(pt[1] - z[1])) \
                / ((y[1] - z[1])*(x[0] - z[0]) + (z[0] - y[0])*(x[1] - z[1]))
            beta = ((z[1] - x[1])*(pt[0] - z[0]) + (x[0] - z[0])*(pt[1] - z[1])) \
                / ((y[1] - z[1])*(x[0] - z[0]) + (z[0] - y[0])*(x[1] - z[1]))
        except ZeroDivisionError:
            return False
        gamma = 1.0 - alpha - beta
        return alpha > .0 and beta > .0 and gamma > .0

    hull_vertices = []
    for v in points:
        for x, y, z in combinations(points, 3):
            if isPtInside(v, x, y, z):
                break
        else:
            hull_vertices.append(v)
    return hull_vertices

def main():
    points = [tuple(map(float, raw_input().split())) for _ in xrange(int(raw_input()))]
    print convexhull(points)

if __name__ == '__main__':
    main()
