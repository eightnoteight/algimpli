


def lcs(X, Y):
    mat = [[0] * (len(Y) + 1) for x in xrange(len(X) + 1)]
    for x in xrange(len(X)):
        for y in xrange(len(Y)):
            if X[x] == Y[y]:
                mat[x][y] = mat[x - 1][y - 1] + 1
            else:
                mat[x][y] = max(mat[x - 1][y], mat[x][y - 1])
    return mat[-1][-1]


print lcs(raw_input(), raw_input())
