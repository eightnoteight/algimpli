#!/usr/bin/env python
# -*- encoding: utf-8 -*-


def median(arr1, arr2):
    lo1 = 0
    hi1 = len(arr1)
    lo2 = 0
    hi2 = len(arr2)
    while hi1 - lo1 > 4 and hi2 - lo2 > 4:
        m1 = arr1[(hi1 + lo1) // 2] if (hi1 + lo1) % 2 else ((arr1[(hi1 + lo1) // 2] + arr1[(hi1 + lo1) // 2 - 1]) / 2.0)
        m2 = arr2[(hi2 + lo2) // 2] if (hi2 + lo2) % 2 else ((arr2[(hi2 + lo2) // 2] + arr2[(hi2 + lo2) // 2 - 1]) / 2.0)
        if m1 == m2:
            return m1
        elif m1 < m2:
            lo1 = (hi1 + lo1) // 2
            hi2 = (hi2 + lo2) // 2 + 1
        else:
            lo2 = (hi2 + lo2) // 2
            hi1 = (hi1 + lo1) // 2 + 1

    tmp = arr1[lo1: hi1] + arr2[lo2: hi2]
    tmp.sort()
    print tmp
    return tmp[len(tmp) // 2] if len(tmp) % 2 else ((tmp[len(tmp) // 2] + tmp[len(tmp) // 2 - 1]) / 2.0)
