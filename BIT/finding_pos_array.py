#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
import sys
sys.stdin = open('/tmp/spojtest.in', 'r')


class bit(object):
    def __init__(self, size):
        self.size = size + 1
        self.arr = [0]*(size + 1)

    def insert(self, val):
        size = self.size
        arr = self.arr
        while val < size:
            arr[val] += 1
            val += val & -val

    def find(self, val):
        arr = self.arr
        n = 0
        while val > 0:
            n += arr[val]
            val -= val & -val
        return n

def main():
    ourarr = bit(10)
    for x in [5, 3, 6, 2, 4, 1]:
        print(ourarr.find(x))
        ourarr.insert(x)

main()
