#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin,star-args
from collections import defaultdict
from math import acos, sqrt, degrees, pi

def docdist(doc1, doc2):
    doc1counter = defaultdict(int)
    doc2counter = defaultdict(int)
    for word in doc1.split():
        doc1counter[word] += 1
    for word in doc2.split():
        doc2counter[word] += 1
    numer = 0
    for word, x in doc1counter.iteritems():
        numer += x*doc2counter[word]
    return acos(numer / (sqrt(sum(doc1counter.values()))*sqrt(sum(doc2counter.values())))) * (200.0 / pi)

print docdist(open(raw_input(), "r").read(), open(raw_input(), "r").read())
