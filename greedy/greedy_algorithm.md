# Greedy Algorithms

### form of psedo code
    algorithm greedy(inputs):
        Solution = None
        for input in OptimizedOrdering(inputs):
            if isFeasible(Solution, input):
                Solution = Union(Solution, input)
        return Solution

the important routines to observe in a greedy algorithm are *Union*,
*OptimizedOrdering*, *Feasible*


### Standard Problems
- Activity Selection Problem
- Fractional Knapsack Problem
- Kruskal's Minimum Spanning Tree algorithm
- Prim's Minimum Spanning Tree algorithm
- Dijkstra's Algorithm
- Travelling Salesman Problem
- Huffman Coding
- connect n ropes with minimum cost


#### Problem #1.
Suppose you have unlimited supply of 1-Rs, 2-Rs, 5-Rs, 10-Rs. then find the
minimum number of coins required such that the sum of the coins is equal to N?

(Note: also try to solve the problem if you have an unlimited supply
of array of coins)

#### Problem #2
You are given some array of tasks with start and end times of the task. and you
have unlimited supply of machines and the problem is to find the minimum number
of machines required to schedule all of the tasks...

#### Problem #3
You are given some array of tasks with start and end times of the task. and you
have A machine and the problem is to find the maximum number of tasks that can
be scheduled in the given machine and also find which tasks ?

#### Problem #4
Container Loading, There are some weights which are containerized into same
sized containers. the problem is to find the maximum number of containers that
can be loaded into ONE ship given the ship's capacity is C. and also find which
containers..

#### Problem #5
Container Loading, There are some weights which are containerized into same
sized containers. the problem is to find the maximum number of containers that
can be loaded into TWO ship given their capacities are C1 and C2 respectively.
and also find which containers..

#### Problem #6
Container Loading, There are some weights which are containerized into same
sized containers. the problem is to find the maximum number of containers that
can be loaded into N number of ship's given their capacities C1, C2, .... Cn
and also find which containers..

#### Problem #7
We are given `n` tasks to perform in sequence. Suppose that task i needs
T[i] units of time. If the tasks are done in the order of 1, 2, .... n, then
task i completes at time C[i] = Sum(T[i] for i=1...n). The Average Completion
Time(ACT) is Sum(C[i] for i=1...n)/n.
The Problem is to find the ordering of the tasks, so that the Average Completion
Time will be minimum...

#### Problem #8
ACT as defined in Problem #7
The problem is to find the distribution of tasks between TWO workers such that
the ACT is minimum.

#### Problem #9
Fractional Knapsack Problem

#### Problem #10

