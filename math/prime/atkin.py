#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111,R0912

from math import sqrt, ceil


def atkin(n):
    sieve = [0]*n
    sqn = int(ceil(sqrt(n)))

    for x in xrange(1, int(ceil(sqrt(n / 4.0)))):
        for y in xrange(1, int(sqrt(n - 4*x*x)) + 2, 2):
            t = 4*x*x + y*y
            #  if t % 60 in (1, 13, 17, 29, 37, 41, 49, 53)
            if t < n and ((t % 12 == 1) or (t % 12 == 5)):
                if sieve[t]:
                    sieve[t] = 0
                else:
                    sieve[t] = t

    for x in xrange(1, int(ceil(sqrt(n / 3.0))), 2):
        for y in xrange(2, int(sqrt(n - 3*x*x)) + 2, 2):
            t = 3*x*x + y*y
            # if t % 60 in (7, 19, 31, 43)
            if t < n and (t % 12 == 7):
                if sieve[t]:
                    sieve[t] = 0
                else:
                    sieve[t] = t

    for x in xrange(1, int(ceil(sqrt(n / 2.0)))):
        for y in xrange(1, x):
            t = 3*x*x - y*y
            # if t > 0 and t % 60 in (11, 23, 47, 59)
            if t < n and t % 12 == 11:
                if sieve[t]:
                    sieve[t] = 0
                else:
                    sieve[t] = t

    sieve[2], sieve[3], sieve[5] = 2, 3, 5
    sieve[4: n: 4] = [0]*int(ceil((n - 4) / 4.0))
    sieve[9: n: 18] = [0]*int(ceil((n - 9) / 18.0))
    for _ in xrange(1, int(ceil((sqn + 1) / 6.0)) + 1):
        x = 6 * _ - 1
        if sieve[x]:
            sieve[x*x: n: 2*x*x] = [0]*int(ceil((n - x*x) / float(2*x*x)))
        x = 6 * _ + 1
        if sieve[x]:
            sieve[x*x: n: 2*x*x] = [0]*int(ceil((n - x*x) / float(2*x*x)))
    return filter(None, sieve)
