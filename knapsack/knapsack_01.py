#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def knapsack_nW(weights, costs, W):
    n = len(weights)
    ks = [[0]*(W + 1) for _ in xrange(n + 1)]
    for i in xrange(1, n + 1):
        for j in xrange(1, W + 1):
            if weights[i - 1] > j:
                ks[i][j] = ks[i - 1][j]
            else:
                ks[i][j] = max(ks[i - 1][j], ks[i - 1][j - weights[i - 1]] + costs[i - 1])
    return ks[n][W]

def knapsack(weights, costs, W):
    n = len(weights)
    dp = [0] * (W + 1)
    for i in xrange(1, n + 1):
        for j in xrange(W, 0, -1):
            if weights[i - 1] <= j:
                dp[j] = max(dp[j], dp[j - weights[i - 1]] + costs[i - 1])
    return dp[W]
