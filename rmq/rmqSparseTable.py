#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin

def generateLogTable(n):
    """
    ::args::
        n ⟶ size of the log table
    ::returns::
        log base 2 table
    ::note::
        using logtable is efficient for large
          number of queries than using math.log approximation.
        math.log easily takes 8 - 20 iteration for each query.
    """
    logt = [0, 0]
    for x in xrange(2, n):
        logt.append(logt[x >> 1] + 1)
    return logt

class rangeMinimumQuery(object):
    """
    rangeMinimumQuery using sparse table approach
    Complexity:
        preprocessing step => O(nlogn)
        query step => O(1)
    """
    def __init__(self, arr):
        self.arr = arr
        n = len(arr)
        self.logt = generateLogTable(n + 1)
        self.rmq = self.generateSparseTable(n, self.logt)

    def __getitem__(self, slx):
        """
        ::args::
            slx ⟶ slice object denoting the range
        ::returns::
            min(arr[i: j])
        """
        i, j, _ = slx.indices(len(self.arr))
        if slx.step is not None:
            raise ValueError(
                "the range minimum query slice musn't contain a step{}".format(slx.step))
        k = self.logt[j - i]
        # print self.rmq, j - (1 << k), k
        return min(
            self.arr[self.rmq[i][k]],
            self.arr[self.rmq[j - (1 << k)][k]])

    def query(self, i, j):
        """
        ::args::
            i ⟶ starting index of the range
            j ⟶ ending index of the range (excluded)
        ::returns::
            min(arr[i: j])
        """
        k = self.logt[j - i]
        if self.arr[self.rmq[i][k]] < self.arr[self.rmq[j - (1 << k)][k]]:
            return self.rmq[i][k]
        else:
            return self.arr[self.rmq[j - (1 << k)][k]]

    def generateSparseTable(self, n, logt):
        rmq = [[0]*(1 + logt[n]) for _ in xrange(n)]
        for x in xrange(n):
            rmq[x][0] = x
        for x in xrange(n - 1, -1, -1):
            for y in xrange(1, logt[n - x] + 1):
                if self.arr[rmq[x][y - 1]] < self.arr[rmq[x + (1 << (y - 1))][y - 1]]:
                    rmq[x][y] = rmq[x][y - 1]
                else:
                    rmq[x][y] = rmq[x + (1 << (y - 1))][y - 1]
        return rmq

def main():
    n, m = map(int, stdin.readline().split())
    arr = map(int, stdin.readline().split())
    # print arr
    rmq = rangeMinimumQuery(arr)
    for line in stdin.readlines():
        l, r = map(int, line.split())
        print rmq[l: r + 1]

main()
