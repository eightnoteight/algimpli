#include <cmath>
#include <limits>
#include <iostream>
#include <vector>

using namespace std;


template<class arrtype, class type>
class RMQ
{
    arrtype& arr;
    type* tree;
    size_t size;
private:
    type constructTree(size_t lo, size_t hi, size_t ind)
    {
        if ((hi - lo) == 1)
        {
            tree[ind] = lo;
            return tree[ind];
        }
        size_t mid = lo + ((hi - lo) / 2);
        type tmp1, tmp2;
        tmp1 = constructTree(lo, mid, 2*ind + 1);
        tmp2 = constructTree(mid, hi, 2*ind + 2);
        if (arr[tmp1] <= arr[tmp2])
            tree[ind] = tmp1;
        else
            tree[ind] = tmp2;
        return tree[ind];
    }
    type callQuery(size_t beg, size_t end, size_t qbeg, size_t qend, size_t ind)
    {
        if ((beg >= qbeg) and (end <= qend))
            return tree[ind];

        if (((beg < qbeg) and (end <= qbeg)) or ((beg >= qend) and (end > qend)))
            return -1;

        size_t mid = beg + ((end - beg) / 2);
        type tmp1, tmp2;
        tmp1 = callQuery(beg, mid, qbeg, qend, 2*ind + 1);
        tmp2 = callQuery(mid, end, qbeg, qend, 2*ind + 2);
        if ((tmp1 == -1) or (tmp2 == -1))
            return tmp1 + tmp2 + 1; // resulting value !!!!
        if (arr[tmp1] <= arr[tmp2])
            return tmp1;
        else
            return tmp2;
    }
public:
    RMQ(arrtype& array, size_t array_size) : arr(array)
    {
        size = array_size;
        tree = new type[size_t(2*(pow(2, ceil(log2(size)))) + 1)];
        constructTree(0, size, 0);
    }
    type query(size_t lo, size_t hi)
    {
        return callQuery(0, size, lo, hi, 0);
    }
};


int main(int argc, char const *argv[])
{
    vector<int> v;
    int size;
    cin >> size;
    for (int i = 0; i < size; ++i)
    {
        int tmp;
        cin >> tmp;
        v.push_back(tmp);
    }
    cout << "check point 1" << endl;
    RMQ<vector<int>, int> rmq(v, size);
    int q;
    cin >> q;
    for (int i = 0; i < q; ++i)
    {
        int lo, hi;
        cin >> lo;
        cin >> hi;
        cout << rmq.query(lo, hi) << endl;
    }
    return 0;
}
