#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin,star-args
#------------ for tests
import random
import time
#------------


def peakfinding_______1D_On(arr):
    """
        :arr: -> array of elements, i.e One-Dimension(1-D)
        :Complexity: -> O(n)
    """
    if len(arr) == 1:
        return arr[0]
    if arr[0] >= arr[1]:
        return arr[0], 0
    if arr[-1] >= arr[-2]:
        return arr[-1], len(arr) - 1
    for x in xrange(1, len(arr) - 1):
        if arr[x] >= arr[x - 1] and arr[x] >= arr[x + 1]:
            return arr[x], x
    assert False  # the algorithm musn't reach to this point,
                  # because there will always be a peak of
                  # type >= of their neighbours

def peakfinding_______1D_Ologn(arr):
    """
        :arr: -> array of elements, i.e One-Dimension(1-D)
        :Complexity: -> O(log n)
    """
    if len(arr) == 1:
        return arr[0], 0
    if arr[0] >= arr[1]:
        return arr[0], 0
    if arr[-1] >= arr[-2]:
        return arr[-1], len(arr) - 1

    lo, hi = 1, len(arr) - 2
    while lo <= hi:
        mid = lo + ((hi - lo) // 2)
        if arr[mid] >= arr[mid - 1] and arr[mid] >= arr[mid + 1]:
            return arr[mid], mid
        elif arr[mid] >= arr[mid - 1]:
            lo = mid
        else:
            hi = mid
    assert False  # the algorithm musn't reach to this point,
                  # because there will always be a peak of
                  # type >= of their neighbours


def peakfinding_______2D_On2(matrix):
    peak = (float('-inf'), (-1, -1))
    for x in xrange(len(matrix)):
        for y in xrange(len(matrix[x])):
            peak = max(peak, (matrix[x][y], (x, y)))
    return peak

def peakfinding_______2D_Onlogn(matrix):
    pass

def peakfinding_______2D_Ologn(matrix):

    def peakfinding_______2D_On2(matrix, lo1, lo2, hi1, hi2):
        peak = (float('-inf'), (-1, -1))
        for x in xrange(lo1, hi1):
            for y in xrange(lo2, hi2):
                peak = max(peak, (matrix[x][y], (x, y)))
        return peak

    if len(matrix) == 1:
        peak, pos = peakfinding_______1D_Ologn(matrix[0])
        return (peak, (0, pos))
    lo, hi = 0, len(matrix) - 1
    while lo < hi:
        mid = lo + ((hi - lo) // 2)
        localpeak, pos = peakfinding_______1D_Ologn(matrix[mid])
        if hi - lo < 3:
            return peakfinding_______2D_On2(matrix, lo, 0, hi, len(matrix[0]))
        elif matrix[mid][pos] >= matrix[mid - 1][pos] and matrix[mid][pos] >= matrix[mid + 1][pos]:
            return (matrix[mid][pos], (mid, pos))
        elif matrix[mid][pos] >= matrix[mid][pos - 1]:
            lo = mid
        else:
            hi = mid
    assert False  # the algorithm musn't reach to this point,
                  # because there will always be a peak of
                  # type >= of their neighbours


def tests():
    arr = [random.randrange(10) for _ in xrange(10**2)]
    val, pos = peakfinding_______1D_Ologn(range(10**2))
    print(val, pos, range(10**2)[max(pos - 1, 0): min(pos + 2, 10**2)])

    val, pos = peakfinding_______1D_On(range(10**2))
    print(val, pos, range(10**2)[max(pos - 1, 0): min(pos + 2, 10**2)])
    print peakfinding_______2D_Ologn([
        [0, 0, 9, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 0],
        [0, 2, 0, 0, 0, 0, 0],
        [0, 3, 0, 0, 0, 0, 0],
        [0, 5, 0, 0, 0, 0, 0],
        [0, 4, 0, 0, 0, 0, 0],
    ])


if __name__ == '__main__':
    tests()
