#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin


def dfsAdjMat(adjMat, start):
    """
    :param:
        graph - Adjacency Matrix
    """
    stack = [start]
    visited = set()
    while stack:
        node = stack.pop()
        if node in visited:
            continue
        visited.add(node)
        stack.extend(
            [child for child, n_con in enumerate(
                adjMat[node]) if n_con > 0 and child not in visited])


def dfsAdjList(adjList, start):
    """
    :param:
        adjList - Adjecency List
        start - node to start the dfs
    """
    stack = [start]
    visited = set()
    while stack:
        node = stack.pop()
        if node in visited:
            continue
        visited.add(node)
        # a previous check improves the timelimit a lot
        stack.extend([x for x in adjList[node] if x not in visited])
