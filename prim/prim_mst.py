#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from heapq import heappush, heappop


def prim_directed(graph):
    """
    prims algorithm on a directed graph, slower than the undirected.
    :param graph: adjacency linked list of graph
     [
        [(1, w1), (2, w2), (3, w3)],  # 0
        [(0, w4), (2, w5)],  # 1
        []  # 2
     ]
    :return: (adjacency linked list of minimum spanning tree, mst weight sum)
    """
    visited = {0, }
    cuts = []
    mst = [set() for _ in xrange(len(graph))]
    mstsum = 0
    nv = len(graph)
    for v in xrange(nv):
        for cut in graph[v]:  # the edges of visited or cuts
            heappush(cuts, (cut[1], v, cut[0]))
        while len(visited) != nv and cuts:
            cut = heappop(cuts)
            if cut[2] in visited and cut[1] in visited:
                continue
            visited.add(cut[2])
            visited.add(cut[1])
            mst[cut[1]].add((cut[2], cut[0]))
            mstsum += cut[0]
            for nextcut in graph[cut[2]]:
                heappush(cuts, (nextcut[1], cut[2], nextcut[0]))

    return mst, mstsum


def prim(graph, nv):
    visited = {0, }
    cuts = []
    mst = [set() for _ in xrange(nv)]
    mstsum = 0
    for elem in graph[0]:
        heappush(cuts, (elem[1], 0, elem[0]))
    for _ in xrange(nv - 1):
        while True:
            minimum_cut = heappop(cuts)
            if minimum_cut[2] in visited:
                continue
            visited.add(minimum_cut[2])
            mstsum += minimum_cut[0]
            mst[minimum_cut[1]].add((minimum_cut[2], minimum_cut[0]))
            mst[minimum_cut[2]].add((minimum_cut[1], minimum_cut[0]))
            break
        for cut in graph[minimum_cut[2]]:
            heappush(cuts, (cut[1], minimum_cut[2], cut[0]))
    return mst, mstsum


def main():
    """
    :stdin:
        takes stdin in the format of
        V E
        src1 dest1 weight1
        src2 dest2 weight2
        .    .     .
        .    .     .
        .    .     .
        srcV destV weightE
    :stdout:
        mst, mstsum
    :return:
        None
    """
    nv, ne = map(int, raw_input().split())
    graph = [set() for _ in xrange(nv)]
    for _ in xrange(ne):
        src, dest, weight = map(int, raw_input().split())
        graph[src].add((dest, weight))
        graph[dest].add((src, weight))
    print prim(graph, nv)

if __name__ == '__main__':
    main()