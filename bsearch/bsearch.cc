#include <bits/stdc++.h>
#define endl                        ('\n')
#define i32inf                      (0x7fffffff)
#define i32_inf                     (-0x7fffffff-1)
#define i64inf                      (0x7fffffffffffffff)
#define i64_inf                     (-0x7fffffffffffffff-1)
#define ui32inf                     (0xffffffffu)
#define ui64inf                     (0xffffffffffffffffu)
#define bitcounti32                 __builtin_popcount
#define bitcounti64                 __builtin_popcountll
using namespace std;

template<class Function>
int32_t bsearch(int32_t lo, int32_t hi, Function check) {
    while(lo < hi) {
        int32_t mid = lo + ((hi - lo) / 2);
        if (check(mid))
            hi = mid;
        else
            lo = mid + 1;
    }
    if (!check(lo))
        throw runtime_error("bsearch: Couldn't Locate the target. program termination authorised!");
    return lo;
}

int main() {
    vector<int32_t> arr = {0, 1, 2, 3, 4, 5, 6, 7, 8, 10};
    int32_t find = 10;
    cout << bsearch(0, arr.size(), [&](int32_t loc) -> bool {
        return arr[loc] >= find;
    }) << endl;
    return 0;
}
