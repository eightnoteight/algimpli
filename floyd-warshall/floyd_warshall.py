#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from copy import deepcopy

def warshall(adjMat, n):
    dp = deepcopy(adjMat)
    for k in xrange(n):
        for i in xrange(n):
            for j in xrange(n):
                if dp[i][k] + dp[k][j] < dp[i][j]:
                    dp[i][j] = dp[i][k] + dp[k][j]
    return dp
